//
//  UniversityDetailsViewModelTests.swift
//  universities
//
//  Created by Arturs on 22/12/2018.
//  Copyright © 2018 u6. All rights reserved.
//

import XCTest
import MapKit
@testable import universities

class UniversityDetailsViewModelTests: XCTestCase {
    
    func testAddressSearchForModel() {
        
        let objectModel = UNVObjectModel(objectID:"1", andTitle:"СПбГУ")
        let viewModel = UNVUniversityDetailsViewModel(universityModel: objectModel)
        
        let expectation = self.expectation(description: "Search")
        
        let mapView = MKMapView()
        viewModel.searchForUniversity(withinRegion: mapView.region, completion: ({
            expectation.fulfill()
        }))
        
        waitForExpectations(timeout: 5, handler: nil)
        if viewModel.universityMapItem == nil {
            XCTAssert(false, "Expected failure when unable to find university on map")
        }
    }
}
