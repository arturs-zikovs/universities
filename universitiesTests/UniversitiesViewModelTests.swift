//
//  UniversitiesViewModelTests.swift
//  universities
//
//  Created by Arturs on 22/12/2018.
//  Copyright © 2018 u6. All rights reserved.
//

import XCTest

class UniversitiesViewModelTests: XCTestCase {
    
    func testDataDownloadCycle() {
        
        //Preparing model
        let viewModel = UNVUniversitiesViewModel()
        
        let expectationCountries = self.expectation(description: "CountriesLoad")
        
        viewModel.loadCountriesData({ _ in
            expectationCountries.fulfill()
        })
        
        waitForExpectations(timeout: 5, handler: nil)
        if viewModel.countriesArray.count == 0 {
            XCTAssert(false, "Expected failure when empty countries downloaded")
        }
        
        let expectationCities = self.expectation(description: "CitiesLoad")
        
        viewModel.loadCities(forCountryID:"1", completion:{ _ in
            expectationCities.fulfill()
        })
        
        waitForExpectations(timeout: 5, handler: nil)
        if viewModel.citiesArray.count == 0 {
            XCTAssert(false, "Expected failure when empty cities downloaded")
        }
        
        let expectationUniversities = self.expectation(description: "UniversitiesLoad")
        
        viewModel.loadUniversities(forCountryID:"1", andCityID:"2", completion:{ _ in
            expectationUniversities.fulfill()
        })
        
        waitForExpectations(timeout: 5, handler: nil)
        if viewModel.universitiesArray.count == 0 {
            XCTAssert(false, "Expected failure when empty universities downloaded")
        }
    }
    
    func testCorrectDataParsing() {
        
        //getting correct data
        guard let data = FileManager.readJson(forResource: "sample") else {
            XCTAssert(false, "Can't get data from sample.json")
            return
        }
        
        var jsonResponse: Any = {}
        do {
            jsonResponse = try JSONSerialization.jsonObject(with: data, options: [])
        } catch _ {
            XCTAssert(false, "Unable to parse sample.json")
        }
        
        //Preparing model
        let viewModel = UNVUniversitiesViewModel()
        let resultArray: NSMutableArray = []
        
        // expected to fill array with data
        viewModel.parseResponse(jsonResponse, into: resultArray)
        
        if resultArray.count == 0 {
            XCTAssert(false, "Expected failure when empty array")
        }
    }
    
    func testWrongKeyInResponse() {
        
        // giving a wrong dictionary
        let dictionary = ["wrongKey" : 123 as AnyObject]
        
        let viewModel = UNVUniversitiesViewModel()
        let resultArray: NSMutableArray = []
        
        // expected to return empty array
        viewModel.parseResponse(dictionary, into: resultArray)
        
        if resultArray.count > 0 {
            XCTAssert(false, "Expected failure when wrong data")
        }
    }
}

extension FileManager {
    
    static func readJson(forResource fileName: String ) -> Data? {
    
        let bundle = Bundle(for: UniversitiesViewModelTests.self)
        if let path = bundle.path(forResource: fileName, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                return data
            } catch {
                // handle error
            }
        }
        
        return nil
    }
}
