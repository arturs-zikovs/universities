//
//  UNVValueSelectionTextFieldDelegate.h
//  universities
//
//  Created by Arturs on 22/12/2018.
//  Copyright © 2018 u6. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class UNVObjectModel;

@protocol UNVValueSelectionTextFieldDelegate <NSObject>

@optional

- (void)textField:(UITextField *)textField didSelectedValue:(UNVObjectModel *)value;

@end

NS_ASSUME_NONNULL_END
