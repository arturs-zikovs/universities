//
//  UNVValueSelectionTextField.m
//  universities
//
//  Created by Arturs on 22/12/2018.
//  Copyright © 2018 u6. All rights reserved.
//

#import "UNVValueSelectionTextField.h"
#import "UNVObjectModel.h"

@interface UNVValueSelectionTextField () <UIPickerViewDelegate, UIPickerViewDataSource>

@property (nonatomic) NSArray *_selectableValues;
@property (nonatomic) UNVObjectModel *_selectedValue;

@end

@implementation UNVValueSelectionTextField

#pragma mark - Lifecycle

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    
    return self;
}

- (void)setup {
    [super setup];
    
    self.selectableValues = @[];
    self.selectedValue = nil;
    
    self.delegate = self;
}

#pragma mark - Initial date representation

- (UNVObjectModel *)selectedValue {
    return self._selectedValue;
}

- (void)setSelectedValue:(UNVObjectModel *)selectedValue {
    
    if (!selectedValue) {
        self.enabled = NO;
        self.text = @"";
    } else {
        self.enabled = YES;
        self.text = selectedValue.title;
        self._selectedValue = selectedValue;
    }
}

- (NSArray *)selectableValues {
    return self._selectableValues;
}

- (void)setSelectableValues:(NSArray *)selectableValues {
    
    self._selectableValues = selectableValues;
    
    if (selectableValues.count) {
        self.selectedValue = selectableValues[0];
    } else {
        self.selectedValue = nil;
    }
}

#pragma mark - Protocol <UITextFieldDelegate>

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    UIPickerView *pickerView = [UIPickerView new];
    pickerView.delegate = self;
    pickerView.dataSource = self;
    pickerView.backgroundColor = UIColor.whiteColor;
    textField.inputView = pickerView;
    
    if (self.selectedValue) {
        [pickerView selectRow:(NSInteger)[self.selectableValues indexOfObject:self.selectedValue]
                  inComponent:0
                     animated:NO];
    }
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if ([self.valueSelectionDelegate respondsToSelector:@selector(textField:didSelectedValue:)]) {
        [self.valueSelectionDelegate textField:self didSelectedValue:self.selectedValue];
    }
}

#pragma mark - Protocol <UIPickerViewDelegate>

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return (NSInteger)self.selectableValues.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    UNVObjectModel *selectedObject = self.selectableValues[(NSUInteger)row];
    return selectedObject.title;
}

- (void)pickerView:(UIPickerView *)pickerView
      didSelectRow:(NSInteger)row
       inComponent:(NSInteger)component {
    
    self.selectedValue = self.selectableValues[(NSUInteger)row];
}

@end
