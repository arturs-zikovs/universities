//
//  UNVValueSelectionTextField.h
//  universities
//
//  Created by Arturs on 22/12/2018.
//  Copyright © 2018 u6. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UNVDismissableTextField.h"
#import "UNVValueSelectionTextFieldDelegate.h"

@class UNVObjectModel;

@interface UNVValueSelectionTextField : UNVDismissableTextField <UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet id<UNVValueSelectionTextFieldDelegate> valueSelectionDelegate;

@property (nonatomic, assign) NSArray *selectableValues;
@property (nonatomic, assign) UNVObjectModel *selectedValue;

- (void)setup;

@end

