//
//  UNVDismissableTextField.h
//  universities
//
//  Created by Arturs on 22/12/2018.
//  Copyright © 2018 u6. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol UNVDismissableTextFieldDelegate;

@class UNVCloseKeyboardAccessoryView;

@interface UNVDismissableTextField : UITextField <UITextFieldDelegate>

@property (nonatomic, nullable, weak) IBOutlet id<UNVDismissableTextFieldDelegate> dismissableFieldDelegate;

@property (nonatomic, strong) UNVCloseKeyboardAccessoryView *accessoryView;

- (void)setup;

@end

NS_ASSUME_NONNULL_END
