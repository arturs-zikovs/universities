//
//  UNVDismissableTextField.m
//  universities
//
//  Created by Arturs on 22/12/2018.
//  Copyright © 2018 u6. All rights reserved.
//

#import "UNVDismissableTextField.h"

#import "UNVCloseKeyboardAccessoryView.h"
#import "UNVDismissableTextFieldDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@implementation UNVDismissableTextField

#pragma mark - Lifecycle

- (nullable instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    
    return self;
}

- (void)setup {
    _accessoryView = [self createAccessoryView];
    self.inputAccessoryView = _accessoryView;

	self.delegate = self;
    [self layoutIfNeeded];
}

#pragma mark - Private

- (UNVCloseKeyboardAccessoryView *)createAccessoryView {
    UNVCloseKeyboardAccessoryView *view = [UNVCloseKeyboardAccessoryView new];
    return view;
}

#pragma mark - Protocol <UITextFieldDelegate>

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	if ([self.dismissableFieldDelegate respondsToSelector:@selector(textFieldDidTapReturnKey:)]) {
		[self.dismissableFieldDelegate textFieldDidTapReturnKey:self];
	}
	return YES;
}

@end

NS_ASSUME_NONNULL_END
