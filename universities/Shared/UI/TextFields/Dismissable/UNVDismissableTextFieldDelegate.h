//
//  UNVDismissableTextFieldDelegate.h
//  universities
//
//  Created by Arturs on 22/12/2018.
//  Copyright © 2018 u6. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UNVDismissableTextField;

@protocol UNVDismissableTextFieldDelegate <NSObject>

@optional

- (void)textFieldDidTapReturnKey:(UNVDismissableTextField *)textField;

@end
