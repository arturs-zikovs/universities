//
//  UNVCloseKeyboardAccessoryView.h
//  universities
//
//  Created by Arturs on 22/12/2018.
//  Copyright © 2018 u6. All rights reserved.
//

#import "UNVCloseKeyboardAccessoryView.h"

#import <PureLayout/PureLayout.h>

static CGFloat const UNVCloseKeyboardAccessoryViewHeight = 44.0;

@interface UNVCloseKeyboardAccessoryView ()

@property (nonatomic, assign) BOOL didSetupConstraints;

@end

@implementation UNVCloseKeyboardAccessoryView

#pragma mark - Lifecycle

- (nullable instancetype)initWithCoder:(NSCoder *)aDecoder {
	self = [super initWithCoder:aDecoder];
	if (self) {
		[self setup];
	}

	return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
	self = [super initWithFrame:frame];
	if (self) {
		[self setup];
	}

	return self;
}

- (void)setup {
	self.translatesAutoresizingMaskIntoConstraints = NO;
	self.clipsToBounds = YES;
	self.backgroundColor = UIColor.whiteColor;

	_closeButton = [self createCloseButton];
	[self addSubview:_closeButton];
}

#pragma mark - Private

- (UIButton *)createCloseButton {
	UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
	button.translatesAutoresizingMaskIntoConstraints = NO;
	button.clipsToBounds = YES;
	button.backgroundColor = UIColor.clearColor;
	[button setTitle:@"Done"
			forState:UIControlStateNormal];
	[button addTarget:self
			   action:@selector(closeKeyboard:)
	 forControlEvents:UIControlEventTouchUpInside];
	return button;
}

- (void)setupBaseConstrainsts {
	[self autoSetDimension:ALDimensionHeight toSize:UNVCloseKeyboardAccessoryViewHeight];
}

- (void)setupConstrainstsForCloseButton {
	[self.closeButton autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self];
	[self.closeButton autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:self];
	[self.closeButton autoPinEdge:ALEdgeTrailing toEdge:ALEdgeTrailing ofView:self withOffset:-14.0];
	[self.closeButton autoSetDimension:ALDimensionWidth toSize:44.0];
}

#pragma mark - Public methods
#pragma mark - Layout

+ (BOOL)requiresConstraintBasedLayout {
	return YES;
}

- (CGSize)intrinsicContentSize {
	return CGSizeMake(UIViewNoIntrinsicMetric, UNVCloseKeyboardAccessoryViewHeight);
}

- (UIView *)viewForFirstBaselineLayout {
	return self;
}

- (UIView *)viewForLastBaselineLayout {
	return self;
}

- (void)updateConstraints {
	if (!self.didSetupConstraints) {
		[self setupConstraints];
		self.didSetupConstraints = YES;
	}

	[super updateConstraints];
}

- (void)setupConstraints {
	[self setupBaseConstrainsts];
	[self setupConstrainstsForCloseButton];
}

#pragma mark - Actions

- (void)closeKeyboard:(id)sender {
	[[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder)
											   to:nil
											 from:nil
										 forEvent:nil];
}

@end
