//
//  AppDelegate.h
//  universities
//
//  Created by Arturs on 22/12/2018.
//  Copyright © 2018 u6. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

