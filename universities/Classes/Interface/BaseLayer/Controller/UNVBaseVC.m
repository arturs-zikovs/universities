//
//  UNVBaseVC.m
//  universities
//
//  Created by Arturs on 22/12/2018.
//  Copyright © 2018 u6. All rights reserved.
//

#import "UNVBaseVC.h"
#import "UNVObjectModel.h"

#import "UNVUniversitySelectionTVC.h"
#import "UNVUniversitySelectionDelegate.h"

#import "universities-Swift.h"

static NSString *const kSegueIdentifierUniversityDetails = @"kSegueUniversityDetails";
static NSString *const kSegueIdentifierUniversitySelection = @"kSegueUniversitySelection";

@interface UNVBaseVC () <UNVUniversitySelectionDelegate>

//Properties
@property (nonatomic) UNVObjectModel *selectedUniversity;

//IBOutlets
@property (weak, nonatomic) IBOutlet UIButton *searchButton;

@end

@implementation UNVBaseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqual:kSegueIdentifierUniversityDetails]) {
        
        //Details view
        UNVUniversityDetailsVC *detailsVC = segue.destinationViewController;
        detailsVC.selectedUniversity = self.selectedUniversity;
        
    } else if ([segue.identifier isEqual:kSegueIdentifierUniversitySelection]) {
        
        //Embed view
        UNVUniversitySelectionTVC *selectionTVC = segue.destinationViewController;
        selectionTVC.universitySelectionDelegate = self;
    }
}

#pragma mark - UniversitySelection delegate methods

- (void)didDeselectUnivercity {
    self.searchButton.enabled = NO;
}

- (void)didSelectUnivercity:(UNVObjectModel *)university {
    self.searchButton.enabled = YES;
    self.selectedUniversity = university;
}

@end
