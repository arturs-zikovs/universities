//
//  UNVUniversitiesViewModelConstants.h
//  universities
//
//  Created by Arturs on 22/12/2018.
//  Copyright © 2018 u6. All rights reserved.
//

#ifndef UNVUniversitiesViewModelConstants_h
#define UNVUniversitiesViewModelConstants_h

static NSString *const kVKEndpointURL = @"https://api.vk.com/method/%@?access_token=fb25064efb25064efb25064ec6fb42ac7effb25fb25064ea73b17f60ccce94d0740164b&v=5.92&count=300";

static NSString *const kVKEndpointCountries = @"database.getCountries";
static NSString *const kVKEndpointCities = @"database.getCities";
static NSString *const kVKEndpointUniversities = @"database.getUniversities";

static NSString *const kVKRequestKeyNeedAll = @"need_all";
static NSString *const kVKRequestKeyCountryID = @"country_id";
static NSString *const kVKRequestKeyCityID = @"city_id";

static NSString *const kVKResponseKeyResponse = @"response";
static NSString *const kVKResponseKeyItems = @"items";
static NSString *const kVKResponseKeyID = @"id";
static NSString *const kVKResponseKeyTitle = @"title";

#endif /* UNVUniversitiesViewModelConstants_h */
