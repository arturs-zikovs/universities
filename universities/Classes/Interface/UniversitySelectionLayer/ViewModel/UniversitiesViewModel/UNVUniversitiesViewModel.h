//
//  UNVUniversitiesViewModel.h
//  universities
//
//  Created by Arturs on 22/12/2018.
//  Copyright © 2018 u6. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef void(^UNVUniversitiesLoadCompletionBlock)(NSError *error);

@interface UNVUniversitiesViewModel : NSObject

@property (nonatomic) NSMutableArray *countriesArray;
@property (nonatomic) NSMutableArray *citiesArray;
@property (nonatomic) NSMutableArray *universitiesArray;

- (instancetype)initWithCountriesDataLoad:(UNVUniversitiesLoadCompletionBlock)completion;

- (void)loadCountriesData:(UNVUniversitiesLoadCompletionBlock)completion;
- (void)loadCitiesForCountryID:(NSString *)countryID
                    completion:(UNVUniversitiesLoadCompletionBlock)completion;

- (void)loadUniversitiesForCountryID:(NSString *)countryID
                           andCityID:(NSString *)cityID
                          completion:(UNVUniversitiesLoadCompletionBlock)completion;

#pragma mark - Exposing for testing
- (void)parseResponse:(id)response intoArray:(NSMutableArray *)destinationArray;

@end

