//
//  UNVUniversitiesViewModel.m
//  universities
//
//  Created by Arturs on 22/12/2018.
//  Copyright © 2018 u6. All rights reserved.
//

#import "UNVUniversitiesViewModel.h"
#import "UNVUniversitiesViewModelConstants.h"
#import "UNVObjectModel.h"

#import "AFNetworking.h"

@implementation UNVUniversitiesViewModel

#pragma mark - Lifecycle

- (instancetype)init {
    self = [super init];
    if (!self) return nil;
    
    self.countriesArray = [@[] mutableCopy];
    self.citiesArray = [@[] mutableCopy];
    self.universitiesArray = [@[] mutableCopy];
    
    return self;
}

- (instancetype)initWithCountriesDataLoad:(UNVUniversitiesLoadCompletionBlock)completion {
    self = [self init];
    if (!self) return nil;
    
    [self loadCountriesData:completion];
    
    return self;
}

#pragma mark - Countries management

- (void)loadCountriesData:(UNVUniversitiesLoadCompletionBlock)completion {
    
    NSString *countryAPIEndpoint = [NSString stringWithFormat:kVKEndpointURL, kVKEndpointCountries];
    NSDictionary *params = @{kVKRequestKeyNeedAll: @(1)};
    
    [self loadDataIntoArray:self.countriesArray withURL:countryAPIEndpoint parameters:params andCompletionHandler:completion];
}

#pragma mark - City management

- (void)loadCitiesForCountryID:(NSString *)countryID
                    completion:(UNVUniversitiesLoadCompletionBlock)completion {
    
    NSString *cityAPIEndpoint = [NSString stringWithFormat:kVKEndpointURL, kVKEndpointCities];
    NSDictionary *params = @{kVKRequestKeyNeedAll: @(0),
                             kVKRequestKeyCountryID: countryID};
    
    [self loadDataIntoArray:self.citiesArray withURL:cityAPIEndpoint parameters:params andCompletionHandler:completion];
}

#pragma mark - University management

- (void)loadUniversitiesForCountryID:(NSString *)countryID
                           andCityID:(NSString *)cityID
                          completion:(UNVUniversitiesLoadCompletionBlock)completion {
    
    NSString *universitiesAPIEndpoint = [NSString stringWithFormat:kVKEndpointURL, kVKEndpointUniversities];
    NSDictionary *params = @{kVKRequestKeyCountryID: @"1",
                             kVKRequestKeyCityID: @"2"};

    [self loadDataIntoArray:self.universitiesArray withURL:universitiesAPIEndpoint parameters:params andCompletionHandler:completion];
}

#pragma mark - Internal

- (void)loadDataIntoArray:(NSMutableArray *)destinationArray
                  withURL:(NSString *)url
               parameters:(NSDictionary *)parameters
     andCompletionHandler:(UNVUniversitiesLoadCompletionBlock)completion {
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];

    [manager GET:url parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        [self parseResponse:responseObject intoArray:destinationArray];
        completion(nil);
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        completion(error);
    }];
}

- (void)parseResponse:(id)response intoArray:(NSMutableArray *)destinationArray {
    
    if (destinationArray) {
        [destinationArray removeAllObjects];
        
        //TODO: Mapping with ObjectMapper
        if ([response objectForKey:kVKResponseKeyResponse]) {
            NSDictionary *responseDictionary = response[kVKResponseKeyResponse];
            if ([responseDictionary objectForKey:kVKResponseKeyItems]) {
                for (NSDictionary *item in responseDictionary[kVKResponseKeyItems]) {
                    [destinationArray addObject:[[UNVObjectModel alloc] initWithObjectID:item[kVKResponseKeyID]
                                                                                andTitle:item[kVKResponseKeyTitle]]];
                }
            }
        }
    }
}

@end
