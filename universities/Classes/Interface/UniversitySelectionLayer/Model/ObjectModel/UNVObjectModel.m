//
//  UNVObjectModel.m
//  universities
//
//  Created by Arturs on 22/12/2018.
//  Copyright © 2018 u6. All rights reserved.
//

#import "UNVObjectModel.h"

@implementation UNVObjectModel

- (instancetype)initWithObjectID:(NSString *)objectID andTitle:(NSString *)title {
    self = [super init];
    if (!self) return nil;
    
    _objectID = [objectID copy];
    _title = [title copy];
    
    return self;
}

@end
