//
//  UNVObjectModel.h
//  universities
//
//  Created by Arturs on 22/12/2018.
//  Copyright © 2018 u6. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UNVObjectModel : NSObject 

@property (nonatomic, copy, readonly) NSString *objectID;
@property (nonatomic, copy, readonly) NSString *title;

- (instancetype)initWithObjectID:(NSString *)objectID andTitle:(NSString *)title;

@end

