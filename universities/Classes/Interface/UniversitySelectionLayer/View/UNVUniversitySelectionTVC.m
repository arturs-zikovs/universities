//
//  UNVUniversitySelectionTVC.m
//  universities
//
//  Created by Arturs on 22/12/2018.
//  Copyright © 2018 u6. All rights reserved.
//

#import "UNVUniversitySelectionTVC.h"
#import "UNVUniversitySelectionDelegate.h"

#import "UNVUniversitiesViewModel.h"
#import "UNVObjectModel.h"

#import "UNVValueSelectionTextField.h"

@interface UNVUniversitySelectionTVC () <UNVValueSelectionTextFieldDelegate>

//Properties
@property (nonatomic, strong) UNVUniversitiesViewModel *viewModel;

//IBOutlets
@property (weak, nonatomic) IBOutlet UNVValueSelectionTextField *countryTextField;
@property (weak, nonatomic) IBOutlet UNVValueSelectionTextField *cityTextField;
@property (weak, nonatomic) IBOutlet UNVValueSelectionTextField *universityTextField;

@end

@implementation UNVUniversitySelectionTVC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self performInitialViewSetup];
    [self setupViewModel];
}

- (void)performInitialViewSetup {
    self.tableView.tableFooterView = [UIView new];
}

#pragma mark - View models lifecycle

- (void)setupViewModel {
    
    self.viewModel = [[UNVUniversitiesViewModel alloc] initWithCountriesDataLoad:^(NSError *error) {
        [self updateTextField:self.countryTextField withValues:self.viewModel.countriesArray];
        
        [self.countryTextField becomeFirstResponder];
    }];
}

#pragma mark - TextField delegate methods

- (void)textField:(UITextField *)textField didSelectedValue:(UNVObjectModel *)value {
    if ([textField isEqual:self.countryTextField]) {
        self.cityTextField.selectedValue = nil;
        self.universityTextField.selectedValue = nil;
        if ([self.universitySelectionDelegate respondsToSelector:@selector(didDeselectUnivercity)]) {
            [self.universitySelectionDelegate didDeselectUnivercity];
        }
        
        [self.viewModel loadCitiesForCountryID:self.countryTextField.selectedValue.objectID
                                    completion:^(NSError *error) {
                                        
            [self updateTextField:self.cityTextField withValues:self.viewModel.citiesArray];
            [self.cityTextField becomeFirstResponder];
        }];
        
    } else if ([textField isEqual:self.cityTextField]) {
        self.universityTextField.selectedValue = nil;
        if ([self.universitySelectionDelegate respondsToSelector:@selector(didDeselectUnivercity)]) {
            [self.universitySelectionDelegate didDeselectUnivercity];
        }
        
        [self.viewModel loadUniversitiesForCountryID:self.countryTextField.selectedValue.objectID
                                           andCityID:self.cityTextField.selectedValue.objectID
                                          completion:^(NSError *error) {
                                              
            if (!self.viewModel.universitiesArray.count) {
                [self displayAlertWithTitle:@"No universities found in this city!" andMessage:@"Select another city to proceed"];
                [self.cityTextField becomeFirstResponder];
            } else {
                [self updateTextField:self.universityTextField withValues:self.viewModel.universitiesArray];
                
                if ([self.universitySelectionDelegate respondsToSelector:@selector(didSelectUnivercity:)]) {
                  [self.universitySelectionDelegate didSelectUnivercity:self.universityTextField.selectedValue];
                }
            }
        }];
    } else if ([textField isEqual:self.universityTextField]) {
        
        if ([self.universitySelectionDelegate respondsToSelector:@selector(didSelectUnivercity:)]) {
            [self.universitySelectionDelegate didSelectUnivercity:self.universityTextField.selectedValue];
        }
    }
}

#pragma mark - Support methods

- (void)updateTextField:(UNVValueSelectionTextField *)textField withValues:(NSArray *)valuesToSelectFrom {
    textField.selectableValues = valuesToSelectFrom;
}

- (void)displayAlertWithTitle:(NSString *)title andMessage:(NSString *)message {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAlert = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:okAlert];
    
    [self presentViewController:alert animated:YES completion:nil];
}

@end
