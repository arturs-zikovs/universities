//
//  UNVUniversityDetailsViewModel.swift
//  universities
//
//  Created by Arturs on 22/12/2018.
//  Copyright © 2018 u6. All rights reserved.
//

import MapKit

class UNVUniversityDetailsViewModel : NSObject {
    
    //Properties
    private var universityModel: UNVObjectModel?
    var universityMapItem: MKMapItem?
    
    //MARK: Lifecycle

    init(universityModel: UNVObjectModel?) {
        
        self.universityModel = universityModel
    }
    
    //MARK: University search
    
    func searchForUniversity(withinRegion region: MKCoordinateRegion, completion: @escaping (() -> Void)) {
        
        let request = MKLocalSearch.Request()
        request.naturalLanguageQuery = universityModel?.title
        request.region = region
        let search = MKLocalSearch(request: request)
        search.start { [weak self] response, _ in
            guard let response = response else {
                completion()
                return
            }
            
            if let mapItem = response.mapItems.first {
                self?.universityMapItem = mapItem
            } else {
                print("Address search failed")
            }
            
            completion()
        }
    }
    
    func universityAddressString() -> String {
        
        let street = self.universityMapItem?.placemark.thoroughfare ?? ""
        let streetNumber = self.universityMapItem?.placemark.subThoroughfare ?? ""
        let zipCode = self.universityMapItem?.placemark.postalCode ?? ""
        let city = self.universityMapItem?.placemark.locality ?? ""
        let country = self.universityMapItem?.placemark.country ?? ""
        
        //TODO: Reimplement to bind together only prefilled address data
        let addressString = street + " " + streetNumber + ", " + zipCode + " " + city + ", " + country
        return addressString;
    }
}
