//
//  UNVUniversityDetailsVC.swift
//  universities
//
//  Created by Arturs on 22/12/2018.
//  Copyright © 2018 u6. All rights reserved.
//

import UIKit
import MapKit

@objc class UNVUniversityDetailsVC : UIViewController {
    
    //Properties
    @objc var selectedUniversity: UNVObjectModel?
    var viewModel: UNVUniversityDetailsViewModel?
    
    //IBOutlets
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var locationLabel: UILabel!
    
    //MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViewModel()
        
        //Updating visuals
        self.navigationItem.title = "Map";
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "backArrow").withRenderingMode(.alwaysOriginal), style: UIBarButtonItem.Style.plain, target: navigationController, action: #selector(UINavigationController.popViewController(animated:)))
    }
    
    //MARK: Viewmodel setup
    
    func setupViewModel() {
        viewModel = UNVUniversityDetailsViewModel(universityModel: self.selectedUniversity)
        viewModel?.searchForUniversity(withinRegion: mapView.region, completion: { [weak self] in
            if (self?.viewModel?.universityMapItem != nil) {
                self?.updateMapWithRequestedData()
                self?.updateAddressLabel()
            } else {
                //TODO: Display error
            }
        })
    }
    
    //MARK: Interface updates
    
    func updateMapWithRequestedData() {

        if let coordinate = self.viewModel?.universityMapItem?.placemark.location?.coordinate {
            let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
            let region = MKCoordinateRegion(center: coordinate, span: span)
            mapView.setRegion(region, animated: true)
            
            let annotation = MKPointAnnotation()
            annotation.coordinate = coordinate
            annotation.title = selectedUniversity?.title
            mapView.addAnnotation(annotation)
        }
    }
    
    func updateAddressLabel() {

        locationLabel.text = self.viewModel?.universityAddressString()
    }
    
    //MARK: IBActions
    @IBAction func directionsButtonTapped(_ sender: Any) {
        
        self.viewModel?.universityMapItem?.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving])
    }
}
